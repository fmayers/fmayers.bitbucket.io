	


document.querySelector('.burger').addEventListener('click', function(){
	console.log(document.querySelector('nav'))
	document.querySelector('nav').classList.toggle('open');
})

	/*****************************
	CAROUSELS
	*****************************/
	$('.HeroCarousel').slick({
		autoplay: true,
		autoplaySpeed: 3000,
		dots: true,
		prevArrow: '<i class="fa fa-chevron-left"></i>',
		nextArrow: '<i class="fa fa-chevron-right"></i>'
	});

	$('.SpecialCarousel').slick({
		autoplay: true,
		autoplaySpeed: 3000,
		prevArrow: '<i class="fa fa-chevron-left"></i>',
		nextArrow: '<i class="fa fa-chevron-right"></i>'
	});


	/******************************
	 ADD ANIMATION TO THE TEXT 
	******************************/
	var count = 0;
	var number = 10;
	
	var interval = setInterval(function(){
       count++;
	   if (count === number) { 
	   		clearInterval(interval);
			$('h1 ,h2').addClass('animationActive');		
		}
	}, 50);


	/******************************
	 WINDOW SCROLL 
	******************************/
	
	function scrollToElement(id) {
		var elementPosition = $(id).position();
		console.log(elementPosition)
		window.scrollTo({
		  top: elementPosition.top - 120,
		  left: 0,
		  behavior: 'smooth'
		});
	}


	$('.nav-btn a').click(function(event){

		if( $(this).attr('data-target') ) {
			console.log('true');
			event.preventDefault();
			scrollToElement( $(this).attr('data-target') );
		}

	});

	
	/*****
	 TOP
	******/
	$('#top').click(function(){
		$('html, body').animate({scrollTop: '0px'}, 300);
    });

    /****
	MENU STICKY
	****/
	// When the user scrolls the page, execute myFunction 
	window.onscroll = function() {myFunction()};

	// Get the navbar
	var navbar = document.querySelector(".menu");

	// Get the offset position of the navbar
	var sticky = navbar.offsetTop;

	// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
	function myFunction() {
	  if (window.pageYOffset >= sticky) {
	    navbar.classList.add("sticky")
	  } else {
	    navbar.classList.remove("sticky");
	  }
	}

	/***
	SIMPLE LIGHTBOX
	****/

	if ( $('.myLightbox a').length ) {
		var lightbox = $('.myLightbox a').simpleLightbox({
			captions: true,
			captionSelector: 'self',
		});
	}
